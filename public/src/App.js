import React, { Component } from 'react';
import './App.css';

import Hammer from 'hammerjs';

const feathers = require('feathers/client');
const socketio = require('feathers-socketio/client');
const io = require('socket.io-client');
const _ = require('lodash');

//global variables 
var socket;
var API;

// console.log(window.location.origin)
socket = io(window.location.origin, {
  transports: ['websocket']
});

API = feathers()
  .configure(socketio(socket));

const RemoteService = API.service('remote');


function normalize_values(startA, endA, startB, endB, skip){
  return function(currentA){
      if (currentA < startA && !skip) currentA = startA;
      if (currentA > endA && !skip) currentA = endA;
      
      var currentB = 
          (currentA - startA) * 
          ((endB - startB) / (endA - startA)) + (startB);

      return currentB;
  }
}


class App extends Component {
  constructor(props){
    super(props);

    this.state = {
      gesture: 'init',
      alpha: 0,
      beta: 0,
      gamma: 0,
      beta_norm: 0,
      gamma_norm: 0,
      motion_history: []
    };

    this.rightClick = this.rightClick.bind(this);
    this.buttonClick = this.buttonClick.bind(this);
  }

  componentWillMount() {

  }


  componentDidMount() {
    var that = this;
    var AppBg = document.getElementById('App');

    var beta_norm = normalize_values(-20, 85, 0, 60);
    var gamma_norm = normalize_values(-45, 45, 0, 40);
    
    window.addEventListener('deviceorientation', function(e){
      // that.motion = e;
      // that.setState({
      //   alpha: e.alpha, 
      //   beta: e.beta, 
      //   gamma: e.gamma,
      //   beta_norm: beta_norm(e.beta),
      //   gamma_norm: gamma_norm(e.gamma)
      // });
      window.requestAnimationFrame(()=>updateBgPosition(e));

      
    });
    
    function updateBgPosition(e){
      AppBg.style.backgroundPositionY = -1 * Math.round(beta_norm(e.beta)) + 'px';
      if (e.beta < 85) AppBg.style.backgroundPositionX = -1 * Math.round(gamma_norm(e.gamma)) + 'px';
    };
 
    var mousePad = document.getElementById('mousePad');
    var scrollPad = document.getElementById('scrollPad');
    var scrollPad_horizontal = document.getElementById('scrollPad_horizontal');
    var hammerPad = new Hammer.Manager(mousePad);
    var hammerScrollPad = new Hammer.Manager(scrollPad);
    var hammerScrollHorizontalPad = new Hammer.Manager(scrollPad_horizontal);

    
    hammerPad.add(new Hammer.Tap({event: 'rightclick', taps: 3}));
    hammerPad.add(new Hammer.Tap({event: 'doubletap', taps: 2}));
    hammerPad.add(new Hammer.Tap({event: 'singletap'}));
    hammerPad.add(new Hammer.Pan({direction: Hammer.DIRECTION_ALL}));

    hammerScrollPad.add(new Hammer.Pan({}));
    hammerScrollHorizontalPad.add(new Hammer.Pan({}));


    hammerPad.get('rightclick').recognizeWith(['doubletap', 'singletap']);
    hammerPad.get('doubletap').recognizeWith('singletap');
    hammerPad.get('doubletap').requireFailure('rightclick');
    hammerPad.get('singletap').requireFailure(['doubletap','rightclick']);
    
    var drag_timer,
        drag_flag = false,
        dragging = false;
    
    mousePad.addEventListener('touchstart', function(){
      
      //that.setState({native: 'touchstart'});
      if (drag_flag === true){
        dragging = true;
      } else {
        drag_flag = true;
        clearTimeout(drag_timer);
        
        drag_timer = setTimeout(function(){
          drag_flag = false;
        }, 180);
      }
    });

    //press pressup
    hammerPad.on("panmove panend singletap doubletap rightclick", function(ev) {
        var {
          type,
          velocityX,
          velocityY,
        } = ev;

        //that.setState({type: type});

        if (dragging) {
          RemoteService.create({mouseDrag: {type, velocityX, velocityY}});
        } else {
          RemoteService.create({mouse: {type, velocityX, velocityY}});
        }

        if (type == 'panend') {
          dragging = false;
        };
    });

    var throttled_scroll = _.throttle((velocityY)=>RemoteService.create({scroll: {velocityY}}), 40);
    hammerScrollPad.on("panup pandown", function(ev) {
        var {
          velocityY,
        } = ev;

        throttled_scroll(velocityY);
    });

    var throttled_scroll_horizontal = _.throttle(
      (velocityX)=>RemoteService.create({scrollX: {velocityX}}), 
      40
    );
    hammerScrollHorizontalPad.on("panleft panright", function(ev) {
        var {
          velocityX,
        } = ev;

        throttled_scroll_horizontal(velocityX);
    });
    
    
  }

  rightClick(){
    return ()=>RemoteService.create({mouse: {type: 'rightclick'}});
  }  

  buttonClick(key){
    return ()=>{
      RemoteService.create({keyboard: {key: key}}); 
    }   
  }

  render() {
    return (
      <div className="App" id="App">
        <div>
          {/* <div><button className="tiny ui button esc_btn" onClick={this.buttonClick('escape')}>Esc</button></div> */}
        
        
                  
        <div id="pad">
          <div id="mousePad">
            {/* <p>{this.state.type}</p>
            <p>{this.state.native}</p> */}
            {/* <p>{this.state.alpha.toFixed(2)}</p>
            <p>{this.state.beta.toFixed(2)}</p>
            <p>{this.state.gamma.toFixed(2)}</p>
            <p>{this.state.beta_norm.toFixed(2)}</p>
            <p>{this.state.gamma_norm.toFixed(2)}</p> */}
          </div>
          <div id="scrollPad">
            <i className="caret up icon"></i>
            <i className="caret down icon"></i>
          </div>
          <div id="scrollPad_horizontal">
            <i className="caret left icon"></i>
            <i className="caret right icon"></i>
          </div>
        </div>
        
        <div className="search">
          <div className="ui fluid icon input keyboard">
            <input type="text" onChange={change=>{
              var lastLetter;
              var that = this;
              if (change.target.value && change.target.value.length){
                //string not emtpy
                lastLetter = change.target.value[change.target.value.length - 1];
                if (!this.lastCount) {
                  sendKey();
                } else {
                  if (this.lastCount > change.target.value.length){
                    var diff = this.lastCount - change.target.value.length;
                    for (var i=0;i<diff;i++){
                      that.buttonClick('backspace')();
                    }
                  } else sendKey();
                } 
              } else {
                if (this.lastCount) {
                  for (var y=0;y<this.lastCount;y++){
                    that.buttonClick('backspace')();
                  }
                }
              }

              this.lastCount = change.target.value.length;
              
              function sendKey(){
                that.buttonClick(lastLetter)();
              }

            }} />
            <button className="tiny ui button del" onClick={this.buttonClick('backspace')}>Del</button>
          </div>
          
          <button className="tiny ui button enter" onClick={this.buttonClick('enter')}>Enter</button>
        </div>

        <div className="fastVolume">
          <button className="massive circular ui icon button" onClick={this.buttonClick('audio_vol_down')}>
            <i className="volume down icon"></i>
          </button>
          <button className="large circular ui icon button" onClick={this.buttonClick('audio_mute')}>
            <i className="mute icon"></i>
          </button>
          <button className="massive circular ui icon button" onClick={this.buttonClick('audio_vol_up')}>
            <i className="volume up icon"></i>
          </button>
          
          
          
        </div>
        </div>
        
      </div>
    );
  }
}

export default App;
