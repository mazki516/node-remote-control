// Initializes the `remote` service on path `/remote`
const createService = require('./remote.class.js');
const hooks = require('./remote.hooks');
const filters = require('./remote.filters');

module.exports = function () {
  const app = this;
  const paginate = app.get('paginate');

  const options = {
    name: 'remote',
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/remote', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('remote');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
