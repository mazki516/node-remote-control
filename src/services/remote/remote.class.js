/* eslint-disable no-unused-vars */
var robot = require('robotjs');
var os = require('os');
var mouse_dragging = false;

var platform = os.platform();

function normalize_values(startA, endA, startB, endB){
  return function(currentA){
      //clipping min/max values
      if (currentA < startA) currentA = startA;
      if (currentA > endA) currentA = endA;
      
      var currentB = 
          (currentA - startA) * 
          ((endB - startB) / (endA - startA)) + (startB);

      return currentB;
  }
};

function sinEaseOut(minv, maxv, v) {
    var range = maxv - minv; 
    var normV = ( v - minv ) / range;
    var newNV = normV * (Math.PI/2);
    return range * Math.sin(newNV)  + minv; 
};

const GlobalNormalizer = normalize_values(0,3,40,80);

class Service {
  constructor (options) {
    this.options = options || {};
  }

  find (params) {
    return Promise.resolve([]);
  }

  get (id, params) {
    return Promise.resolve({
      id, text: `A new message with ID: ${id}!`
    });
  }

  create (data, params) {
    // console.log(data);
    var mouse = robot.getMousePos();
    let {x, y} = mouse;

    let mouse_action = data.mouse;
    let mouse_drag_action = data.mouseDrag;
    
    
    let scroll_action = data.scroll;
    let scroll_horizontal_action = data.scrollX;
    let keyboard_action = data.keyboard;
    
    

    function calculate_diff(currentPos, velocity){
      //return currentPos + (velocity * (sinEaseOut(40,80,GlobalNormalizer(Math.abs(velocity)))))
      return currentPos + (velocity * GlobalNormalizer(Math.abs(velocity)))
    }

    if (mouse_action) {
      if (mouse_action.type == 'singletap') robot.mouseClick('left');
      if (mouse_action.type == 'doubletap') robot.mouseClick('left', true);
      if (mouse_action.type == 'rightclick') robot.mouseClick('right');
      
      if (mouse_action.type == 'panend') {
        if (mouse_dragging == true) mouse_dragging = false;
        robot.mouseToggle('up');
      }
      robot.moveMouse(calculate_diff(x,mouse_action.velocityX), calculate_diff(y,mouse_action.velocityY));
    }
    

    if (mouse_drag_action){
      
      if (mouse_dragging === false) {
        mouse_dragging =  true;
        robot.mouseToggle('down');
      }
      

      if (mouse_drag_action.type == 'singletap') robot.mouseClick('left');
      if (mouse_drag_action.type == 'doubletap') robot.mouseClick('left', true);
      if (mouse_drag_action.type == 'rightclick') robot.mouseClick('right');
      if (mouse_drag_action.type == 'panend') {
        if (mouse_dragging == true) mouse_dragging = false;        
        robot.mouseToggle('up');
      }

      robot.dragMouse(calculate_diff(x,mouse_drag_action.velocityX), calculate_diff(y,mouse_drag_action.velocityY));            
    }
      
    

    if (scroll_action && platform == 'linux') {
      if (scroll_action.velocityY > 0) robot.scrollMouse(0, -1);
      else robot.scrollMouse(0, 1);
    } else if (scroll_action) {
      if (scroll_action.velocityY > 0) robot.scrollMouse(-1, 'up');
      else robot.scrollMouse(1, 'down');
    }

    if (scroll_horizontal_action) {
      if (scroll_horizontal_action.velocityX > 0) robot.scrollMouse(-1, 'left');
      else robot.scrollMouse(1, 'right');
    }

    if (keyboard_action) {
      robot.keyTap(keyboard_action.key);
    }

    return true;
  }

  update (id, data, params) {
    return Promise.resolve(data);
  }

  patch (id, data, params) {
    return Promise.resolve(data);
  }

  remove (id, params) {
    return Promise.resolve({ id });
  }
}

module.exports = function (options) {
  return new Service(options);
};

module.exports.Service = Service;
