const remote = require('./remote/remote.service.js');
module.exports = function () {
  const app = this; // eslint-disable-line no-unused-vars
  app.configure(remote);
};
