const assert = require('assert');
const app = require('../../src/app');

describe('\'remote\' service', () => {
  it('registered the service', () => {
    const service = app.service('remote');

    assert.ok(service, 'Registered the service');
  });
});
